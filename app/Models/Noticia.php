<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    //protected $guarded = ['titulo'];// los que no se pueden guardar
    protected $fillable = ['titulo', 'slug', 'contenido', 'tipo'];
    protected $guarded = [];
    // muestra url amigables
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
