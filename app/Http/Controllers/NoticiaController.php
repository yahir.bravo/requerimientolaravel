<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNoticia;
use App\Http\Requests\UpdateNoticia;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NoticiaController extends Controller
{
    // papgina principañ
    public function index() {
        #$noticias = Noticia::all();
        $noticias = Noticia::orderBy('id','desc')->paginate(6);
        return view('noticias.index',compact('noticias'));
    }

    // llamando del formulario de creacion
    public function create() {
        return view('noticias.create');
        //return "Create";
    }

    // generar la lógica de guardadp
    //public function store(Request $request) {
    public function store(StoreNoticia $request) {
        
        if (!$request['tipo']){
            $request['tipo'] = 'Nacional';
        }
        $noticia_new  = new Noticia($request->all());
        $noticia_new['slug'] = Str::slug($noticia_new['titulo'],'-');
        $noticia_new->save();
        return redirect()->route('noticias.index');
    }

    // genera la logica del mostrado de un componente en específico
    public function show(Noticia $noticia){
        return view('noticias.show',compact('noticia'));
    }

    // edición de componentes, retorna el form de la edición
    public function edit($id) {
        $noticia = Noticia::find($id);
        return view('noticias.edit',compact('noticia'));
    }

    // actualización de un registro
    public function update(Noticia $noticia, UpdateNoticia $request) {
        if (!$request['tipo']){
            $request['tipo'] = 'Nacional';
        }
        $noticia->contenido = $request->contenido;
        $noticia->tipo =  $request->tipo;
        $noticia->save();
        return redirect()->route('noticias.show',$noticia);
        
    }

    // destrución del registro
    public function destroy(Noticia $noticia) {
        $noticia->delete();
        return redirect()->route('noticias.index');
    }
}
