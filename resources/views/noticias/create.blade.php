@extends('layouts/base')

@section('tittle', 'Crear Noticias ')

@section('content')
    <h2>Crear Noticia</h2>
    <br>
    <form action=" {{route("noticias.store")}} " method="POST">
        @csrf

        <div class="mb-3">
            <label for="titulo-form" class="form-label">Titulo</label>
            <input type="text" class="form-control" name="titulo" placeholder="Titulo de la noticia"
                value="{{ old('titulo') }}">
            @error('titulo')
                <small class="text-danger">* {{ $message }}</small>
            @enderror
        </div>

        <div class="mb-3">
            <label for="content-form" class="form-label">Contenido</label>
            <textarea class="form-control" name="contenido" cols="40" rows="10">{{ old('contenido') }}</textarea>
            @error('contenido')
                <small class="text-danger">* {{ $message }}</small>
            @enderror
        </div>

        <div class="mb-3">
            <label class="form-label">Tipo de noticia</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="tipo" Value="Internacional" id="check-internacional" {{old('tipo')? 'checked':''}}>
                <label class="form-check-label" for="check-internacional">Internacional</label>
            </div>
            <div class="form-text">Por defecto, la noticia sera de tipo nacional, selecione Internacional para cambiar el tipo</div>
        
        </div>  
        <button type="submit" class="btn btn-primary">Crear noticia</button>
    </form>

@endsection
