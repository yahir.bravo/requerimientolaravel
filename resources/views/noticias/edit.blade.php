@extends('layouts/base')

@section('tittle', 'Crear Noticias ')

@section('content')
    <h3>Edición de Noticia</h2>
    
    <br>
    <form action=" {{route("noticias.update",$noticia)}} " method="POST">
        @csrf
        @method('put')

        <label for="titulo-form" class="form-label">Titulo</label>
        <h1>{{ $noticia->titulo }}</h1>
        

        <div class="mb-3">
            <label for="content-form" class="form-label">Contenido</label>
            <textarea class="form-control" name="contenido" cols="40" rows="10">{{ old('contenido',$noticia->contenido) }}</textarea>
            @error('contenido')
                <small class="text-danger">* {{ $message }}</small>
            @enderror
        </div>

        <div class="mb-3">
            <label class="form-label">Tipo de noticia</label>
            <label for="">{{old('tipo')}}</label>
            <div class="form-check">    
                <input class="form-check-input" type="checkbox" name="tipo" Value="Internacional" id="check-internacional" {{old('tipo',$noticia->tipo) == 'Internacional' ? 'checked':''}}>
                <label class="form-check-label" for="check-internacional">Internacional</label>
            </div>
            <div class="form-text">Por defecto, la noticia sera de tipo nacional, selecione Internacional para cambiar el tipo</div>

        </div>  
        <button type="submit" class="btn btn-primary">Actualizar noticia</button>
    </form>

@endsection
