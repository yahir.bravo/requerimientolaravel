@extends('layouts/base')

@section('tittle', 'Noticias')

@section('content')
    <h2 >Noticias Principales</h2>
    <a class="btn btn-primary" href=" {{route('noticias.create')}} ">Crear noticia</a>
  
    <div class="row row-cols-2 row-cols-md-4 g-4 mt-2">
        @foreach ($noticias as $noticia)
            
            <div class="card m-3 p-1" >
                <div class="card-body">
                    <h4 class="card-title">{{$noticia->titulo}}</h4>
                    <p class="card-text"><small class="text-muted">{{$noticia->tipo}}</small></p>
                </div>
                <div class="card-footer">
                    <a class="btn btn-info" href="{{route('noticias.show',$noticia)}}" class="card-link">Ver noticia</a>
                </div>
                
            </div>
            
        @endforeach
    </div>
    <div class="row">
        {!! $noticias->links("pagination::bootstrap-4") !!}
    </div>
    
@endsection
