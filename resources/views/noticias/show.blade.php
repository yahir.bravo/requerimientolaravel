@extends('layouts/base')

@section('tittle', 'Noticia')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <article class="">
                    
                    <h2 class="">{{ $noticia->titulo }}</h2>
                    
                    <figure class="row mt-3 text-muted   ">
                        <p class="col"> {{ $noticia->created_at }} </p>
                        <figcaption class="col blockquote-footer text-end">
                            <cite >{{ $noticia->tipo}} </cite>
                        </figcaption>
                        <hr>
                    </figure>
                    <div class="flex">
                        <p class="text-justify" style="text-justify: inter-word; text-align: justify; ">
                            {{ $noticia->contenido }}
                        </p>
                    </div>
                </article>
            </div>
            <div class="col-md-2">
                <div class="p-4 align-right">
                    <p class="col">
                        <a class="btn btn-primary" href=" {{route("noticias.edit", $noticia->id)}} ">Editar</a>
                    </p>
                    <form  class="col" action="" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">eliminar</button>
                    </form>
                </div>
            </div>
        </div>
        
    </div>

@endsection
