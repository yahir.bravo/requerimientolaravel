<?php

namespace Database\Factories;

use App\Models\Noticia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class NoticiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noticia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $title = $this->faker->sentence();

        return [
            'titulo' => $title,
            'slug' => Str::slug($title),
            'contenido' => $this->faker->paragraph(),
            'tipo' => $this->faker->randomElement(['Nacional', 'Internacional'])
        ];
    }
}
